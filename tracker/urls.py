from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_home(request):
    return redirect("list_projects")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("accounts/", include("accounts.urls")),
    path("", redirect_home, name="home"),
    path("tasks/", include("tasks.urls")),
]
